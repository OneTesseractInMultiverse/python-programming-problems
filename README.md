# Python Programming Problems


## Problem 1: Binary Matrix Operations

Based on the following python function declaration:

`python
def print_diagonals_for_matrix(matrix_size: int):
    # your code goes here...
`
Fill the required code so that the following invocation is done:

`python
print_diagonals_for_matrix(10)
`

The folowing output is displayed in the standard output:

```bash
1   0   0   0   0   0   0   0   0   1
0   1   0   0   0   0   0   0   1   0
0   0   1   0   0   0   0   1   0   0
0   0   0   1   0   0   1   0   0   0
0   0   0   0   1   1   0   0   0   0
0   0   0   0   1   1   0   0   0   0
0   0   0   1   0   0   1   0   0   0
0   0   1   0   0   0   0   1   0   0
0   1   0   0   0   0   0   0   1   0
1   0   0   0   0   0   0   0   0   1
```


## Problem 2: Binary Matrix: Upper and Lower Triagle

Based on the following python function declaration:

```python
def print_upper_and_lower_triangles_for_matrix(matrix_size: int):
    # your code goes here...
```
Fill the required code so that the following invocation is done:

```python
print_upper_and_lower_triangles_for_matrix(10)
```

The folowing output is displayed in the standard output:

```bash
1   1   1   1   1   1   1   1   1   1
0   1   1   1   1   1   1   1   1   0
0   0   1   1   1   1   1   1   0   0
0   0   0   1   1   1   1   0   0   0
0   0   0   0   1   1   0   0   0   0
0   0   0   0   1   1   0   0   0   0
0   0   0   1   1   1   1   0   0   0
0   0   1   1   1   1   1   1   0   0
0   1   1   1   1   1   1   1   1   0
1   1   1   1   1   1   1   1   1   1
```

